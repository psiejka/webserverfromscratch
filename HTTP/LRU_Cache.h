#pragma once
#include <list>
#include <unordered_map>
#include <string>
#include <optional>
#include <mutex>

template <typename Key, typename Value, size_t cache_size>
class LRUCache
{
public:
	using lru_iterator = typename std::list<std::pair<Key, Value>>::iterator;

	LRUCache() : m_capacity(cache_size) {}
	~LRUCache() = default;
	LRUCache(const LRUCache& cache) = delete;
	LRUCache& operator=(const LRUCache& cache) = delete;
	LRUCache(LRUCache&& cache) = delete;
	LRUCache& operator=(LRUCache&& cache) = delete;

	void Insert(const Key& key, const Value& val) {
		std::lock_guard lock(mutex);

		if (!isCached(key)) {
			if (m_map.size() >= m_capacity) {
				Evict();
			}
			m_elements.push_front(std::make_pair(key, val));
			m_map[key] = m_elements.begin();
		}
		else {
			Touch(key);
			//Should I check for Value of that key? Maybe it has changed?
			m_map[key]->second = val; // value update
		}
	}

	void Erase(const Key& key) {
		std::lock_guard lock(mutex);

		auto item = m_map[key];

		m_elements.erase(item);
		m_map.erase(key);
	}

	bool isCached(const Key& key) {
		std::lock_guard lock(mutex);

		return m_map.find(key) != m_map.end();
	}

	std::optional<Value> Get(const Key& key) {
		std::lock_guard lock(mutex);

		if (isCached(key)) {
			Touch(key);
			return m_map[key]->second;
		}
		return {};
	}

	size_t GetSize() {
		std::lock_guard lock(mutex);

		return m_elements.size();
	}

	size_t GetCapacity(){
		std::lock_guard lock(mutex);

		return m_capacity;
	}

	auto begin() const {
		return m_elements.cbegin();
	}

	auto end() const {
		return m_elements.cend();
	}

private:
	auto GetEvictCandidate() {
		return --m_elements.end();
	}

	void Evict() {
		auto item = GetEvictCandidate();
		m_map.erase(item->first);
		m_elements.pop_back();
	}

	void Touch(const Key& key) {
		auto item = m_map[key];
		m_elements.splice(m_elements.begin(), m_elements, item, std::next(item));
	}

private:
	std::list<std::pair<Key, Value>> m_elements;
	std::unordered_map<Key, lru_iterator> m_map;
	size_t m_capacity;

	std::recursive_mutex mutex;
};
