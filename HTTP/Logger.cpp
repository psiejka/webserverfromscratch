#include "Logger.h"
#include <string_view>

std::recursive_mutex Logger::mutex;
std::weak_ptr<Logger> Logger::m_logger;

Logger::Logger()
{
}

Logger::~Logger()
{
}

std::shared_ptr<Logger> Logger::GetInstance()
{
	std::lock_guard lock(mutex);

	std::shared_ptr<Logger> instance = m_logger.lock();
	if (instance == nullptr) {
		instance.reset(new Logger());
		m_logger = instance;
	}
	return instance;
}

void Logger::Log(const std::string & format)
{
	std::lock_guard lock(Logger::mutex);
	std::cout << format;
}

void Logger::Log(std::string_view format)
{
	std::lock_guard lock(Logger::mutex);
	std::cout << format;
}

void Logger::Log(const char* format)
{
	std::lock_guard lock(Logger::mutex);
	std::cout << format;
}

void Logger::Log(const char format)
{
	std::lock_guard lock(Logger::mutex);
	std::cout << format;
}
