#pragma once
#include <memory>
#include <mutex>
#include <iostream>
#include <string>

//Singleton class that provides safe messages printing (to the standard output) in a multithreaded environment.
class Logger
{
public:
	~Logger();

	//Returns instance of this class
	static std::shared_ptr<Logger> GetInstance();

	//Template method that takes variable number of arguments
	template<typename T, typename... Args>
	void Log(const char* format, T value, Args... args)
	{
		std::lock_guard lock(mutex);

		for (; *format != '\0'; format++) {
			if (*format == '%') {
				Log(value);
				Log(format + 1, args...); 
				return;
			}
			Log(*format);
		}
	}

	//Prints charachter to the standard output
	void Log(const char format);
	//Prints array of charachters to the standard output
	void Log(const char* format);
	//Prints string to the standard output
	void Log(const std::string& format);
	//Prints string to the standard output
	void Log(std::string_view format);

protected:
	static std::recursive_mutex mutex;

private:
	Logger();
	static std::weak_ptr<Logger> m_logger;
};
