#include "Request.h"

Request::Request()
{
}

Request::~Request()
{
}

bool Request::Parse(std::string_view request)
{
	std::string_view::const_iterator headersOffset = request.cbegin();
	std::string_view::const_iterator bodyOffset;

	auto lineTerminator_size = request.find(CRLF);
	if (lineTerminator_size > request.length()) {
		return false;
	}
	auto lineTerminator = request.cbegin() + lineTerminator_size;
	if (lineTerminator == request.cend()) {
		return false;
	}
	if (!ParseLine(request.cbegin(), lineTerminator)) {
		return false; //TODO(psiejka): ?????Could be partial success?????
	}
	headersOffset = lineTerminator + CRLF.length();
	auto[success, bodyOff] = m_headers.Parse({ headersOffset, request.end() });
	if (!success) {
		return false; //TODO(psiejka): check if it's true
	}
	bodyOffset = headersOffset + bodyOff; //TODO(psiejka): refactor names

	const auto bodyLength = request.length() - std::distance(request.begin(), bodyOffset);
	//check Content-length header for body length.
	if (m_headers.HasHeader("Content-Length")) {
		//I assume that header value is correct
		//TODO{psiejka): check if header value is a decimal number
		size_t contentLength = std::stoi(m_headers.GetHeaderValue("Content-Length"));

		if (contentLength > bodyLength) {
			return false;
		}

		m_messageBody = std::string(bodyOffset, bodyOffset + contentLength);
	}
	else {
		m_messageBody.clear();
	}
	return true;
}

void Request::Clear()
{
	m_requestMethod.clear();
	m_uriTarget.Clear();
	m_protocol.clear();
	m_headers.Clear();
	m_messageBody.clear();
}

bool Request::ParseLine(std::string_view::const_iterator startOfLine, std::string_view::const_iterator endOfLine) {
	/*
	GET /hello.txt HTTP/1.1
	User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3
	Host: www.example.com
	Accept-Language: en, mi
	*/

	//Parse the method
	auto methodDelimiter = std::find(startOfLine, endOfLine, ' ');
	if (methodDelimiter == endOfLine) {
		return false;
	}

	m_requestMethod = std::string(startOfLine, methodDelimiter);
	if (m_requestMethod.empty()) {
		return false;
	}

	//Parse the target URL
	auto targetURLDelimiter = std::find(methodDelimiter + 1, endOfLine, ' ');
	std::string targetURL(methodDelimiter + 1, targetURLDelimiter);
	if (targetURL.length() == 0)
		return false;
	if (!m_uriTarget.Parse(std::move(targetURL))) {
		return false;
	}

	m_protocol = std::string(targetURLDelimiter + 1, endOfLine);
	if (m_protocol != "HTTP/1.1" || m_protocol.length() == 0) {
		return false;
	}
	return true;
}

std::string Request::GetRequestMethod()
{
	return m_requestMethod;
}

Uri Request::GetTargetUri()
{
	return m_uriTarget;
}

std::string Request::GetProtocol()
{
	return m_protocol;
}

Imf Request::GetHeaders()
{
	return m_headers;
}

std::string Request::GetBody()
{
	return m_messageBody;
}