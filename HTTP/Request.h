#pragma once
#include <string>
#include "../IMF/Imf.h"
#include "../URI/Uri.h"

//Class parses the requests from clients
class Request
{
public:
	Request();
	~Request();
	Request(Request& other) = delete;
	Request(Request&& other) = delete;
	Request& operator=(Request& other) = delete;
	Request& operator=(Request&& other) = delete;

public:
	//Starts parsing the request
	bool Parse(std::string_view request);
	//Sets all private members as if they were never used.
	void Clear();

	//Returns request method
	std::string GetRequestMethod();
	//Returns target URI
	Uri GetTargetUri();
	//Returns protocol of the request
	std::string GetProtocol();
	//Returns headers of the request
	Imf GetHeaders();
	//Returns body of the request
	std::string GetBody();

private:
	//Parses the first line of request
	bool ParseLine(std::string_view::const_iterator startOfLine, std::string_view::const_iterator endOfLine);

private:
	std::string m_requestMethod;
	Uri m_uriTarget;
	std::string m_protocol;
	Imf m_headers;
	std::string m_messageBody;

	const std::string CRLF = "\r\n";
};

