#include "Response.h"
#include <sstream>

Response::Response()
{
	Clear();
	ext_tmp = {
		{"js", "text/javascript"},
		{"css", "text/css"},
		{"html", "text/html"},
		{"yaml", "text/yaml"},
		{"png", "image/png"},
		{"css.map", "application/json"},
		{"json", "application/json"},
		{"jpg", "image/jpg"},
		{"jpeg", "image/jpeg"},
		{"xml", "text/xml"},
		{"text", "text/plain"}
	};
}

Response::~Response()
{
}

void Response::Clear()
{
	m_protocol.clear();
	m_statusCode = 0;
	m_statusMessage.clear();
	m_headers.Clear();
	m_body.clear();
}

void Response::SetFirstLine(unsigned int statusCode, std::string_view protocol)
{
	m_protocol = protocol;
	m_statusCode = statusCode;

	//status codes from here: https://www.restapitutorial.com/httpstatuscodes.html
	switch (statusCode) {
		//Informational responses	- 1xx

		//Success					- 2xx
	case 200: m_statusMessage = "OK"; break;

		//Redirection				- 3xx

		//Client error				- 4xx
	case 400: m_statusMessage = "Bad Request"; break;
	case 404: m_statusMessage = "Not Found"; break;

		//Server error				- 5xx

		//Default
	default:
		m_statusMessage = "Unknown"; break;
	}
}

void Response::AddHeader(std::string_view headerName, std::string_view headerValue)
{
	m_headers.AddHeader(headerName, headerValue);
}

void Response::SetBody(std::string_view body)
{
	size_t bodyLength = body.length();
	if (bodyLength > 2) {
		bodyLength += 2;
	}
	AddHeader("Content-Length", std::to_string(bodyLength));
	m_body = body;
}

std::string Response::Generate() {
	std::ostringstream response;
	response << m_protocol << ' ' << m_statusCode << ' ' << m_statusMessage << "\r\n";
	response << m_headers.GenerateHeaders();
	response << m_body << "\r\n";
	return response.str();
}

Response& Response::Make404() {
	SetFirstLine(404);
	SetBody("<center>Uuuu... Oh, NO!<br><p><h1>404</h1></p></center>");
	AddHeader("Content-Type", ext_tmp.find("html")->second);

	return *this;
}

Response& Response::Make200(std::string_view body, std::string_view extension) {
	if (extension.empty()) {
		extension = "text";
	}
	SetFirstLine(200);
	SetBody(body);
	AddHeader("Content-Type", ext_tmp.find(static_cast<std::string>(extension))->second);

	return *this;
}