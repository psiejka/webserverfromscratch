#pragma once
#include <string>
#include "../IMF/Imf.h"
#include <map>

//TODO(psiejka):
// AddHeader() vs SetHeader() - the latter check if new header already is in the container

//Class that generates response for request
class Response
{
public:
	Response();
	~Response();
	Response(Response& other) = delete;
	Response(Response&& other) = delete;
	Response& operator=(Response& other) = delete;
	Response& operator=(Response&& other) = delete;

	//Sets all private members as if they were never used.
	void Clear();

	//Sets first line of response - PROTOCOL, STATUS_CODE and STATUS_MESSAGE
	void SetFirstLine(unsigned int statusCode, std::string_view protocol = "HTTP/1.1");
	//Adds header to the list of headers
	void AddHeader(std::string_view headerName, std::string_view headerValue);
	//Sets body of response
	void SetBody(std::string_view body);
	//Generates string from variables;
	std::string Generate();

	//CONVENIENCE METHODS
	//Creates 404 - NOT FOUND response.
	Response& Make404();
	//Creates 200 - OK response.
	Response& Make200(std::string_view body = "", std::string_view extension = "");

private:
	std::string m_protocol;
	unsigned int m_statusCode;
	std::string m_statusMessage;
	Imf m_headers;
	std::string m_body;

	std::map<std::string, std::string> ext_tmp;
};

