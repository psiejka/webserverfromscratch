#include "Logger.h"
#include "Server.h"
#include "Response.h"
#include "Request.h"
#include <thread>

#include <fstream>

#include <map>

#include "LRU_Cache.h"

Server::Server() : m_stop{ false }, m_staticContentPath{ "" }
{
}

Server::~Server()
{
	Stop();
	if (m_activeConnectionsManager.joinable()) {
		m_activeConnectionsManager.join();
	}
	if (m_serverThread.joinable()) {
		m_serverThread.join();
	}
}

void Server::Run(uint16_t port)
{
	m_activeConnectionsManager = std::move(std::thread(&Server::RemoveClosedConnections, this));
	m_serverThread = std::move(std::thread(&Server::ServerProcessor, this, port));
}

void Server::RemoveClosedConnections()
{
	while (!m_stop) {
		if (m_activeConnections.size() > 2) {
			std::lock_guard lock(mutex);
			for (auto t = m_activeConnections.begin(); t != m_activeConnections.end(); ) {
				if (!((*t)->IsActive())) {
					t = m_activeConnections.erase(t);
				}
				else {
					++t;
				}
			}
		}
		std::this_thread::sleep_for(std::chrono::seconds(5));
	}
}

void Server::ServerProcessor(uint16_t port)
{
	std::shared_ptr<TransportLayer> transportLayer = std::make_shared<TransportLayer>();
	transportLayer->SetUp(port, 5,
		[this](const std::string& request)
		{
			return ProcessReceivedData(request); }
	);
	AddNewConnection(transportLayer);

	while (!m_stop) {
		std::shared_ptr<TransportLayer> guest = transportLayer->Accept();
		if (guest != nullptr) {
			AddNewConnection(guest);
			guest->StartProcess();
			Logger::GetInstance()->Log("Vector size: %.\n", std::to_string(m_activeConnections.size()));
		}
	}
}

void Server::Stop()
{
	m_stop = true;
	for (auto t = m_activeConnections.begin(); t != m_activeConnections.end(); ++t) {
		(*t)->TerminateThread();
	}
}

void Server::AddNewConnection(std::shared_ptr<TransportLayer> connection)
{
	std::lock_guard lock(mutex);
	m_activeConnections.push_back(connection);
}

//TODO(psiejka):
// - Create "Live HTTP Header Viewer" that allows to determine content-type -- https://developer.mozilla.org/en-US/docs/Web/Security/Securing_your_site/Configuring_server_MIME_types
std::string Server::ProcessReceivedData(std::string_view request)
{
	static LRUCache<std::string, std::string, 15> cache;

	Logger::GetInstance()->Log("RequestFromClient: \n%\n", request);
	
	static thread_local Request parsedRequest;
	static thread_local Response response;
	parsedRequest.Clear();
	response.Clear();
	if (!parsedRequest.Parse(request)) {
		return "";
	}

	if (parsedRequest.GetRequestMethod() == "GET") {
		if (!cache.isCached(parsedRequest.GetTargetUri().GetPath())) {
			std::ifstream fileHandler(m_staticContentPath + parsedRequest.GetTargetUri().GetPath(), std::ios::binary);

			auto fileExtentionDelimiter = parsedRequest.GetTargetUri().GetPath().find('.');
			std::string fileExtention = { parsedRequest.GetTargetUri().GetPath(), fileExtentionDelimiter + 1 };

			if (!fileHandler) {
				response.Make404();
			}
			else {
				std::string fileContent((std::istreambuf_iterator<char>(fileHandler)),
					(std::istreambuf_iterator<char>()));
				cache.Insert(parsedRequest.GetTargetUri().GetPath(), fileContent);

				response.Make200(fileContent, fileExtention);
			}
		}
		else {
			auto fileExtentionDelimiter = parsedRequest.GetTargetUri().GetPath().find('.');
			std::string fileExtention = { parsedRequest.GetTargetUri().GetPath(), fileExtentionDelimiter + 1 };

			response.Make200(cache.Get(parsedRequest.GetTargetUri().GetPath()).value_or("ERROR"), fileExtention);
		}
	}
	else {
		response.Make404();
	}
	return response.Generate();
}

void Server::SetContentPath(std::string_view path)
{
	m_staticContentPath = path;
}
