#pragma once
#include <memory>
#include <string>
#include "../URI/Uri.h"
#include "../IMF/Imf.h"
#include "TransportLayer.h"
#include <thread>
#include <mutex>
#include <atomic>

/*TODO:
- check if message is not very very very long, like 999999999999999999999999999 bytes long
*/

//Class that starts the server, logs every connection and manages other modules of this project.
class Server
{
public:
	Server();
	~Server();
	Server(Server& other) = delete;
	Server(Server&& other) = delete;
	Server& operator=(Server& other) = delete;
	Server& operator=(Server&& other) = delete;

public:

	//Starts server and connection manager threads
	void Run(uint16_t port);
	//Stops server and closes every open connection
	void Stop();
	//Sets path to the website
	void SetContentPath(std::string_view path);
	//Processes received data from connected client
	virtual std::string ProcessReceivedData(std::string_view request);

private:
	//Adds new connection to the list
	void AddNewConnection(std::shared_ptr<TransportLayer> connection);
	//Removes closed connection
	void RemoveClosedConnections();
	//Listens for connections
	void ServerProcessor(uint16_t port);

private:
	std::string m_staticContentPath;

	std::vector<std::shared_ptr<TransportLayer>> m_activeConnections;
	std::thread m_activeConnectionsManager;
	std::thread m_serverThread;
	std::atomic<bool> m_stop;
	std::mutex mutex;
									
	//line terminator
	const std::string CRLF = "\r\n";
};

