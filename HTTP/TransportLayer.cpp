#include "TransportLayer.h"
#include <string>
#include "Logger.h"

TransportLayer::TransportLayer() : m_socket{ INVALID_SOCKET }, m_stop{ false }, m_clientIP{""}, m_clientPort{ "" }
{
}

TransportLayer::TransportLayer(SOCKET socket, std::function<std::string(const std::string)> processData, std::string_view IP, std::string_view port) 
	: m_socket{ socket }, m_stop{ false }, m_processData{processData}, m_clientIP{ IP }, m_clientPort{ port }
{
}

TransportLayer::~TransportLayer()
{
	if (worker.joinable()) {
		worker.join();
	}
}

bool TransportLayer::SetUp(uint16_t port, int connectionLimit, std::function<std::string(const std::string)> processData)
{
	WSADATA wsaData;
	int results;
	results = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (results != 0) {
		return false;
	}

	addrinfo hints;
	addrinfo* serverinfo;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = IPPROTO_TCP;

	results = GetAddrInfoA(NULL, std::to_string(port).c_str(), &hints, &serverinfo);
	if (results != 0) {
		WSACleanup();
		return false;
	}

	m_socket = socket(serverinfo->ai_family, serverinfo->ai_socktype, serverinfo->ai_protocol);
	if (m_socket == INVALID_SOCKET) {
		FreeAddrInfoA(serverinfo);
		WSACleanup();
		return false;
	}
	/*
		u_long arg = 1;
		ioctlsocket(m_socket, FIONBIO, &arg);*/

	results = bind(m_socket, serverinfo->ai_addr, static_cast<int>(serverinfo->ai_addrlen));
	if (results == SOCKET_ERROR) {
		FreeAddrInfoA(serverinfo);
		closesocket(m_socket);
		WSACleanup();
		return false;
	}

	FreeAddrInfoA(serverinfo);

	results = listen(m_socket, connectionLimit);
	if (results == SOCKET_ERROR) {
		FreeAddrInfoA(serverinfo);
		closesocket(m_socket);
		WSACleanup();
		return false;
	}

	m_processData = processData;

	return true;
}

std::shared_ptr<TransportLayer> TransportLayer::Accept()
{
	sockaddr_in m_connectionInfo;
	int m_addressLength = sizeof(m_connectionInfo);
	SOCKET guestSocket = accept(m_socket, (struct sockaddr *)&m_connectionInfo, &m_addressLength);
	//u_long arg = 1;
	//ioctlsocket(guestSocket, FIONBIO, &arg);
	if (guestSocket == INVALID_SOCKET) {
		int rc = WSAGetLastError();
		if (rc == WSAEWOULDBLOCK) { //it's for non-blocking sockets
			return nullptr;
		}
		else {
			return nullptr;
		}
	}
	Logger::GetInstance()->Log("Server: %:% is connected.\n", inet_ntoa(m_connectionInfo.sin_addr), std::to_string(ntohs(m_connectionInfo.sin_port)));

	//return static_cast<std::shared_ptr<TransportLayerI>>(std::make_shared<TransportLayer>(guestSocket, m_processData));
	return std::make_shared<TransportLayer>(guestSocket, m_processData, inet_ntoa(m_connectionInfo.sin_addr), std::to_string(ntohs(m_connectionInfo.sin_port)));
}

void TransportLayer::StartProcess()
{
	worker = std::move(std::thread(&TransportLayer::Process, this));
}

bool TransportLayer::IsActive()
{
	return !m_stop;
}

//TODO(psiejka): make recvbuf "unlimited"
void TransportLayer::Process() {
	std::string output = "";

	//recv funtion returns 0 on EOF or closed connection , -1 when error and X as number of bytes received
	char recvbuf[4096];
	while (!m_stop) {
		ZeroMemory(&recvbuf, 1);
		int result = recv(m_socket, recvbuf, sizeof(recvbuf), 0);
		switch (result) {
		case 0:
			//client closed the connection
			Logger::GetInstance()->Log("Server: Connection closed: %:%.\n", m_clientIP, m_clientPort);
			m_stop = true;
			break;
		case SOCKET_ERROR:
		{
			Logger::GetInstance()->Log("Server: SOCKET_ERROR: %:%.\n", m_clientIP, m_clientPort);
			m_stop = true;
			break;
		}
		default:
			std::string request = std::string(recvbuf, result);
			std::string response = m_processData(request);
			SendData(response);
		}
	}
}

void TransportLayer::SendData(std::string & message)
{
	while (!message.empty()) {
		const int dataSent = send(m_socket, message.c_str(), static_cast<int>(message.length()), 0);
		if (dataSent > 0) {
			message = message.substr(dataSent, std::distance(message.begin(), message.end()));
		}
		if (dataSent == SOCKET_ERROR) {
			const int lastError = WSAGetLastError();
			if (lastError != WSAEWOULDBLOCK) {
				m_stop = true;
				return;
			}
		}
	}
}

void TransportLayer::TerminateThread() {
	m_stop = true;
	closesocket(m_socket);
}