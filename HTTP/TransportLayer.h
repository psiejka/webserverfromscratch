#pragma once
#include <WinSock2.h>
#include <ws2tcpip.h>
#include <memory>
#include <string>
#include <functional>
#include <thread>

#pragma comment (lib, "Ws2_32.lib")

//This Class manages sockets of clients and server. It provide methods to write to opened sockets and to close them.
class TransportLayer{
public:
	TransportLayer();
	~TransportLayer();
	TransportLayer(SOCKET socket, std::function<std::string(const std::string)> processData, std::string_view IP, std::string_view port);
	TransportLayer(TransportLayer& other) = delete;
	TransportLayer(TransportLayer&& other) = delete;
	TransportLayer& operator=(TransportLayer& other) = delete;
	TransportLayer& operator=(TransportLayer&& other) = delete;

	//Initiates use of Winsock by server process
	bool SetUp(uint16_t port, int connectionLimit, std::function<std::string(const std::string)> processData);
	//Accepts the connection
	std::shared_ptr<TransportLayer> Accept();
	//Send data to connected client
	void SendData(std::string& message);
	//Starts the receiving data from clients
	void StartProcess();
	//Checks if connection is still active
	bool IsActive();
	//Ends connection with client
	void TerminateThread();

private:
	//Receives data from clients in the loop
	void Process();

private:
	SOCKET m_socket;
	bool m_stop;
	std::function<std::string(const std::string)> m_processData;

	std::string m_clientIP;
	std::string m_clientPort;

	std::thread worker;
};