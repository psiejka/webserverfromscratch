#include "pch.h"
#include "../HTTP/Server.h"
#include "../URI/Uri.h"
#include "../HTTP/TransportLayer.h"
#include <fstream>
#include "Response.h"
#include "Request.h"

//
//TEST(XX, e) {
//  EXPECT_EQ(1, 1);
//  EXPECT_TRUE(true);
//}

TEST(ParserTest, GetRequest) {
	const std::string requestMessage =
		"GET /hello.txt HTTP/1.1\r\n"
		"User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3\r\n"
		"Host: www.example.com\r\n"
		"Accept-Language: en, mi\r\n"
		"\r\n";
	Request request;
	const auto result = request.Parse(requestMessage);
	ASSERT_TRUE(result);
	Uri expecterUri;
	expecterUri.Parse("/hello.txt");
	ASSERT_EQ(expecterUri, request.GetTargetUri());

	ASSERT_EQ("GET", request.GetRequestMethod());
	ASSERT_TRUE(request.GetHeaders().HasHeader("User-Agent"));
	ASSERT_EQ("curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3", request.GetHeaders().GetHeaderValue("User-Agent"));

	ASSERT_TRUE(request.GetHeaders().HasHeader("Host"));
	ASSERT_EQ("www.example.com", request.GetHeaders().GetHeaderValue("Host"));

	ASSERT_TRUE(request.GetHeaders().HasHeader("Accept-Language"));
	ASSERT_EQ("en, mi", request.GetHeaders().GetHeaderValue("Accept-Language"));

	ASSERT_TRUE(request.GetBody().empty());
}

TEST(ParserTest, PostRequest) {
	const std::string requestMessage =
		"POST / HTTP/1.1\r\n"
		"Host: www.example.com\r\n"
		"Content-Type: text/plain\r\n"
		"Content-Length: 12\r\n"
		"\r\n"
		"Hello World!\r\n";
	Request request;
	const auto result = request.Parse(requestMessage);
	ASSERT_TRUE(result);
	Uri expecterUri;
	expecterUri.Parse("/");
	ASSERT_EQ(expecterUri, request.GetTargetUri());

	ASSERT_EQ("POST", request.GetRequestMethod());
	ASSERT_TRUE(request.GetHeaders().HasHeader("Host"));
	ASSERT_EQ("www.example.com", request.GetHeaders().GetHeaderValue("Host"));

	ASSERT_TRUE(request.GetHeaders().HasHeader("Content-Type"));
	ASSERT_EQ("text/plain", request.GetHeaders().GetHeaderValue("Content-Type"));

	ASSERT_TRUE(request.GetHeaders().HasHeader("Content-Length"));
	ASSERT_EQ("12", request.GetHeaders().GetHeaderValue("Content-Length"));

	ASSERT_FALSE(request.GetBody().empty());
	ASSERT_EQ("Hello World!", request.GetBody());
}

TEST(ParserTest, InvalidRequest_Method) {
	const std::string requestMessage =
		" /hello.txt HTTP/1.1\r\n"
		"User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3\r\n"
		"Host: www.example.com\r\n"
		"Accept-Language: en, mi\r\n"
		"\r\n";
	Request request;
	const auto result = request.Parse(requestMessage);
	ASSERT_FALSE(result);
}


TEST(ParserTest, InvalidRequest_URI) {
	const std::string requestMessage =
		"GET  HTTP/1.1\r\n"
		"User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3\r\n"
		"Host: www.example.com\r\n"
		"Accept-Language: en, mi\r\n"
		"\r\n";
	Request request;
	const auto result = request.Parse(requestMessage);
	ASSERT_FALSE(result);
}

TEST(ParserTest, InvalidRequest_Protocol) {
	const std::string requestMessage =
		"GET /hello.txt XXX\r\n"
		"User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3\r\n"
		"Host: www.example.com\r\n"
		"Accept-Language: en, mi\r\n"
		"\r\n";
	Request request;
	const auto result = request.Parse(requestMessage);
	ASSERT_FALSE(result);
}

TEST(ParserTest, InvalidRequest_HeaderIncomplete) {
	const std::string requestMessage =
		"GET /hello.txt HTML/1.1\r\n"
		"User-Agent curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3\r\n"
		"Host: www.example.com\r\n"
		"Accept-Language: en, mi\r\n"
		"\r\n";
	Request request;
	const auto result = request.Parse(requestMessage);
	ASSERT_FALSE(result);
}

TEST(ParserTest, InvalidRequest_TooLongHeader) {
	const std::string headerTest = "TooLongHeaderNotTerminated - Upsi: ";
	const std::string lineTooLong(999 - headerTest.length(), 'X');
	const std::string requestMessage =
		"GET /hello.txt HTML/1.1\r\n"
		"User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3\r\n"
		+ headerTest + lineTooLong +
		"Host: www.example.com\r\n"
		"Accept-Language: en, mi\r\n"
		"\r\n";
	Request request;
	const auto result = request.Parse(requestMessage);
	ASSERT_FALSE(result);
}

TEST(ParserTest, InvalidRequest_BodyLengthMissmatch) {
	const std::string requestMessage =
		"POST / HTTP/1.1\r\n"
		"Host: www.example.com\r\n"
		"Content-Type: text/plain\r\n"
		"Content-Length: 130\r\n"
		"\r\n"
		"Hello World!\r\n";
	Request request;
	const auto result = request.Parse(requestMessage);
	ASSERT_FALSE(result);
}


TEST(ParserTest, InvalidRequest_WithoutBodyHeadersAndLineTermination) {
	const std::string requestMessage =
		"POST / HTTP/1.1\r";
	Request request;
	const auto result = request.Parse(requestMessage);
	ASSERT_FALSE(result);
}

TEST(ParserTest, InvalidRequest_WithoutBodyAndLineTermination) {
	const std::string requestMessage =
		"POST / HTTP/1.1\r\n"
		"Host: www.example.com\r";
	Request request;
	const auto result = request.Parse(requestMessage);
	ASSERT_FALSE(result);
}

TEST(ParserTest, RequestWithoutContentLengthHeader) {
	const std::string requestMessage =
		"POST /hello.txt HTTP/1.1\r\n"
		"Host: www.example.com\r\n"
		"Content-Type: text/plain\r\n"
		"\r\n"
		"Hello World!\r\n";
	Request request;
	const auto result = request.Parse(requestMessage);
	ASSERT_TRUE(result);
	Uri expecterUri;
	expecterUri.Parse("/hello.txt");
	ASSERT_EQ(expecterUri, request.GetTargetUri());

	ASSERT_EQ("POST", request.GetRequestMethod());
	ASSERT_TRUE(request.GetHeaders().HasHeader("Host"));
	ASSERT_EQ("www.example.com", request.GetHeaders().GetHeaderValue("Host"));

	ASSERT_TRUE(request.GetHeaders().HasHeader("Content-Type"));
	ASSERT_EQ("text/plain", request.GetHeaders().GetHeaderValue("Content-Type"));

	ASSERT_FALSE(request.GetHeaders().HasHeader("Content-Length"));

	ASSERT_TRUE(request.GetBody().empty());
}

TEST(RequestsProcessingTest, Code404NotFound) {
	const std::string requestMessage =
		"GET /hello.txt HTTP/1.1\r\n"
		"User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3\r\n"
		"Host: www.example.com\r\n"
		"Accept-Language: en, mi\r\n"
		"\r\n";
	Server server;
	server.SetContentPath("www/");

	std::string response = server.ProcessReceivedData(requestMessage);
	ASSERT_FALSE(response.empty());

	const std::string expectedResponse = {
				"HTTP/1.1 404 Not Found\r\n"
				"Content-Length: 57\r\n"
				"Content-Type: text/html\r\n"
				"\r\n"
				"<center>Uuuu... Oh, NO!<br><p><h1>404</h1></p></center>\r\n" };

	ASSERT_EQ(response, expectedResponse);
}

TEST(RequestsProcessingTest, Code200OK)
{
	const std::string path = "www/";
	const std::string requestMessage =
		"GET /index.html HTTP/1.1\r\n"
		"User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3\r\n"
		"Host: www.example.com\r\n"
		"Accept-Language: en, mi\r\n"
		"\r\n";
	Request request;
	Server server;
	server.SetContentPath(path);

	ASSERT_TRUE(request.Parse(requestMessage));
	std::string response = server.ProcessReceivedData(requestMessage);
	ASSERT_FALSE(response.empty());

	std::ifstream fileHandler(path + request.GetTargetUri().GetPath());
	std::string fileContent((std::istreambuf_iterator<char>(fileHandler)),
		(std::istreambuf_iterator<char>()));

	const std::string expectedResponse = 
		"HTTP/1.1 200 OK\r\n" +
		std::string("Content-Length: ") + std::to_string(fileContent.length()+2) + std::string("\r\n") +
		"Content-Type: text/html\r\n" +
		"\r\n" +
		fileContent + "\r\n";

	ASSERT_EQ(response, expectedResponse) << "To get this test to pass You have to create files www/index.html in directory of compiled binary of this test.";
}

//RESPONSE CLASS TESTS//
TEST(ResponseTest, ResponseBuilder) {
	Response response;
	response.SetFirstLine(404);
	response.SetBody("Uuuu... Oh, NO!");
	response.AddHeader("Content-Type", "text/plain");

	const std::string expectedResponse = {
				"HTTP/1.1 404 Not Found\r\n"
				"Content-Length: 17\r\n"
				"Content-Type: text/plain\r\n"
				"\r\n"
				"Uuuu... Oh, NO!\r\n" };

	ASSERT_EQ(response.Generate(), expectedResponse);
}

//override standard method that generates responses for requests

class MyServer : public Server {
public:
	std::string ProcessReceivedData(std::string_view request) {
		const std::string body = "<center>It's my own 404 error.<br><p><h1>404</h1></p></center>";
		const std::string response =
			"HTTP/1.1 404 Not Found\r\n"
			"Content-Length: 64\r\n"
			"Content-Type: text/html\r\n"
			"\r\n"
			+ body + "\r\n";

		return response;
	}
};

TEST(ResponseTest, OverrideProcessReceivedDataMethod) {
	const std::string requestMessage =
		"GET /hello.txt HTTP/1.1\r\n"
		"User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3\r\n"
		"Host: www.example.com\r\n"
		"Accept-Language: en, mi\r\n"
		"\r\n";

	MyServer server;
	auto response = server.ProcessReceivedData(requestMessage);

	const std::string expectedResponseBody = "<center>It's my own 404 error.<br><p><h1>404</h1></p></center>";
	const std::string expectedResponse =
		"HTTP/1.1 404 Not Found\r\n"
		"Content-Length: 64\r\n"
		"Content-Type: text/html\r\n"
		"\r\n"
		+ expectedResponseBody + "\r\n";
	ASSERT_EQ(response, expectedResponse);
}

//LRU CACHE CLASS TESTS//
#include "LRU_Cache.h"

struct LRUCacheTest : public ::testing::Test {
public:
	LRUCache<int, std::string, 5> cache;
};

TEST_F(LRUCacheTest, InCacheAfterInsertion) {
	ASSERT_EQ(cache.GetSize(), 0);
	for (int i = 0; i < 5; i++) {
		cache.Insert(i, std::to_string(i));
		EXPECT_EQ(cache.GetSize(), i + 1);
		EXPECT_TRUE(cache.isCached(i));
	}
}

TEST_F(LRUCacheTest, InCacheAfterEviction) {
	ASSERT_EQ(cache.GetSize(), 0);
	for (int i = 0; i < 10; i++) {
		cache.Insert(i, std::to_string(i));
	}
	EXPECT_EQ(cache.GetSize(), 5);
	for (int i = 5; i < 10; i++) {
		EXPECT_TRUE(cache.isCached(i));
	}
	for (int i = 0; i < 5; i++) {
		EXPECT_FALSE(cache.isCached(i));
	}
}

TEST_F(LRUCacheTest, EraseInserted) {
	cache.Insert(0, "zero");
	cache.Insert(1, "one");
	EXPECT_EQ(cache.GetSize(), 2);
	cache.Erase(0);
	EXPECT_EQ(cache.GetSize(), 1);
	EXPECT_TRUE(cache.isCached(1));
	EXPECT_FALSE(cache.isCached(0));
}

TEST_F(LRUCacheTest, TestCapacityAndSize) {
	EXPECT_EQ(cache.GetCapacity(), 5);
	EXPECT_EQ(cache.GetSize(), 0);
}

TEST_F(LRUCacheTest, GetInsertedElement) {
	cache.Insert(0, "zero");
	cache.Insert(1, "one");
	EXPECT_EQ(cache.Get(0).value(), "zero");
	EXPECT_EQ(cache.Get(1).value(), "one");
}

TEST_F(LRUCacheTest, GetNotInsertedElement) {
	ASSERT_EQ(cache.GetSize(), 0);
	EXPECT_EQ(cache.Get(0).value_or(""), "");
	ASSERT_EQ(cache.GetSize(), 0);
}
