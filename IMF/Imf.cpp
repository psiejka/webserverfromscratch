#include "Imf.h"
#include <sstream>

#include <iostream>

Imf::Imf()
{
}

Imf::~Imf()
{
}

std::vector<Imf::Header> Imf::GetHeaders() const
{
	return m_headers;
}

std::string Imf::GenerateHeaders() const {
	std::ostringstream message;
	for (const auto& header : m_headers) {
		message << header.m_headerName << ": " << header.m_headerValue << CRLF;
		//TODO: if line is too long - fold -> https://tools.ietf.org/html/rfc2822#page-8
	}
	message << CRLF;
	return message.str();
}

bool Imf::HasHeader(const std::string & headerName) const
{
	for (const auto& header : m_headers) {
		if (header.m_headerName == headerName) {
			return true;
		}
	}
	return false;
}

std::string Imf::GetHeaderValue(const std::string & headerName) const
{
	for (const auto& header : m_headers) {
		if (header.m_headerName == headerName) {
			return header.m_headerValue;
		}
	}
	return {};
}

void Imf::AddHeader(const std::string & headerName, const std::string & headerValue)
{
	m_headers.emplace_back(Header(headerName, headerValue));
}

void Imf::AddHeader(std::string_view headerName, std::string_view headerValue)
{
	m_headers.emplace_back(Header(headerName, headerValue));
}

void Imf::AddHeader(const Header& header)
{
	m_headers.emplace_back(header);
}

void Imf::Clear()
{
	m_headers.clear();
}

std::pair<bool, std::pair<std::string, std::string>> Imf::SeparateMessage(std::string::const_iterator start, std::string::const_iterator end, const std::string& message)
{
	auto nameDelimiter = std::find(start, end, ':');
	if (nameDelimiter == message.end()) {
		return { false, {} };
	}
	std::string headerName(start, nameDelimiter);
	std::string headerValue(nameDelimiter + 1, end);
	return { true, {headerName, headerValue} };
}

std::string Imf::RemoveWhitespace(const std::string header)
{
	unsigned int counterFront = 0;
	unsigned int counterBack = 0;

	//Front
	while (header[counterFront] == ' ') {
		counterFront++;
	}
	//Back
	while (header[header.length() - counterBack - 1] == ' ') {
		counterBack++;
	}

	return { header.begin() + counterFront, header.end() - counterBack };
}

std::pair<bool, size_t> Imf::Parse(const std::string& message) {
	//message = message;

	std::string::const_iterator offset = message.begin();
	while (offset != message.end()) {
		auto lineTerminator_size = message.find(CRLF, std::distance(message.begin(), offset));
		if (lineTerminator_size == std::string::npos) {
			return { false, std::distance(message.begin(), offset) };
		}

		auto lineTerminator = message.begin() + lineTerminator_size;
		//if end of message
		if (lineTerminator == message.end()) {
			std::string s_tmp(offset, lineTerminator);
			if (message.length() - s_tmp.length() + CRLF.length() > lineLengthLimit) {
				return { false, std::distance(message.begin(), offset) };
			}
			break;
		}

		//if line is longer than 1000 characters
		if (CRLF.length() + std::string(offset, lineTerminator).length() > lineLengthLimit) {
			return { false, std::distance(message.begin(), offset) };
		}

		//if empty line has been found
		if (lineTerminator == offset) {
			offset += CRLF.length();
			break;
		}

		auto[completed, header] = SeparateMessage(offset, lineTerminator, message);
		//if message couldn't be separated
		if (!completed) {
			return { false, std::distance(message.begin(), offset) };
		}

		offset = lineTerminator + CRLF.length();
		m_headers.emplace_back(header.first, RemoveWhitespace(header.second));
	}

	return { true, std::distance(message.begin(), offset) };
}