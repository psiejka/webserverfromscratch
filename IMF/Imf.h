#pragma once
#include <string>
#include <vector>

//Class that parses headers as described in https://tools.ietf.org/html/rfc2822
class Imf
{
public:
	Imf();
	~Imf();

	//copy constructor
	Imf(const Imf& other) = default;
	//copy assignment operator
	Imf& operator=(const Imf& other) = default;

	//move constructor
	Imf(Imf&& other) = default;
	//move assignment operator
	Imf& operator=(Imf&& other) = default;
public:
	struct Header {
		std::string m_headerName;
		std::string m_headerValue;

		Header(
			std::string_view headerName,
			std::string_view headerValue
		) : m_headerName{ headerName }, m_headerValue{ headerValue }{};
	};

	//Determines headers of the messsage and the offset of message
	std::pair<bool, size_t> Parse(const std::string& message);

	//Returns vector of headers
	std::vector<Imf::Header> GetHeaders() const;

	//Constructs and returns message with headers based on the headers in the vector
	std::string GenerateHeaders() const;

	//Checks if there is a header with given name
	bool HasHeader(const std::string& headerName) const;

	//Returns header value of given header name
	std::string GetHeaderValue(const std::string& headerName) const;

	//Adds header to the container
	void AddHeader(const std::string& headerName, const std::string& headerValue);
	void AddHeader(std::string_view headerName, std::string_view headerValue);
	void AddHeader(const Header& header);

	//Sets all private members as if they were never used.
	void Clear();

private:
	//Takes parts of original message and extract the header name and value
	std::pair<bool, std::pair<std::string, std::string>> SeparateMessage(std::string::const_iterator start, std::string::const_iterator end, const std::string& message);

	//Removes any whitespace at the beginning and end of header
	std::string RemoveWhitespace(const std::string header);
private:
	std::vector<Header> m_headers;

	//line terminator
	const std::string CRLF = "\r\n";
	//line length (with CRLF)
	const size_t lineLengthLimit = 1000; 
};