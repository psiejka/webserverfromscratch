#include "pch.h"
#include "../IMF/Imf.h"
//TEST(TestCaseName, TestName) {
//  EXPECT_EQ(1, 1);
//  EXPECT_TRUE(true);
//}

TEST(InternetMessageFormat, MessageToOnePerson) {
	const std::string rawMessage = (
		"From: John Doe <jdoe@machine.example>\r\n"
		"Sender: Michael Jones <mjones@machine.example>\r\n"
		"To: Mary Smith <mary@example.net>\r\n"
		"Subject: Saying Hello\r\n"
		"Date: Fri, 21 Nov 1997 09 : 55 : 06 - 0600\r\n"
		"Message - ID: <1234@local.machine.example>\r\n"
		"\r\n"
		);
	Imf imf;
	ASSERT_EQ(true, imf.Parse(rawMessage).first);
	const auto headerCollection = imf.GetHeaders();
	const std::vector< Imf::Header > expectedHeaders{
	{"From", "John Doe <jdoe@machine.example>"},
	{"Sender", "Michael Jones <mjones@machine.example>"},
	{"To", "Mary Smith <mary@example.net>"},
	{"Subject", "Saying Hello"},
	{"Date", "Fri, 21 Nov 1997 09 : 55 : 06 - 0600"},
	{"Message - ID", "<1234@local.machine.example>"},
	};
	ASSERT_EQ(expectedHeaders.size(), headerCollection.size());
	for (size_t i = 0; i < expectedHeaders.size(); ++i) {
		ASSERT_EQ(expectedHeaders[i].m_headerName, headerCollection[i].m_headerName);
		ASSERT_EQ(expectedHeaders[i].m_headerValue, headerCollection[i].m_headerValue);
	}
}

TEST(InternetMessageFormat, LineTooLong) {
	const std::string headerTest = "TooLongHeader - Upsi: ";
	const std::string lineTooLong(999 - headerTest.length(), 'X');
	const std::string rawMessage = (
		"From: John Doe <jdoe@machine.example>\r\n"
		"Sender: Michael Jones <mjones@machine.example>\r\n"
		+ headerTest + lineTooLong + "\r\n"
		"\r\n"
		);
	Imf imf;
	ASSERT_EQ(false, imf.Parse(rawMessage).first);
}

TEST(InternetMessageFormat, LineAlmostTooLong) {
	const std::string headerTest = "TooLongHeader - Upsi: ";
	const std::string lineTooLong(998 - headerTest.length(), 'X');
	const std::string rawMessage = (
		"From: John Doe <jdoe@machine.example>\r\n"
		"Sender: Michael Jones <mjones@machine.example>\r\n"
		+ headerTest + lineTooLong + "\r\n"
		"\r\n"
		);
	Imf imf;
	ASSERT_EQ(true, imf.Parse(rawMessage).first);
}

TEST(InternetMessageFormat, LineTooLongNotTerminated) {
	const std::string headerTest = "TooLongHeaderNotTerminated - Upsi: ";
	const std::string lineTooLong(999 - headerTest.length(), 'X');
	const std::string rawMessage = (
		"From: John Doe <jdoe@machine.example>\r\n"
		"Sender: Michael Jones <mjones@machine.example>\r\n"
		+ headerTest + lineTooLong
		+ "\r\n"
		);
	Imf imf;
	ASSERT_EQ(false, imf.Parse(rawMessage).first);
}

TEST(InternetMessageFormat, EmptyMessage) {
	Imf imf;
	ASSERT_EQ(true, imf.Parse("").first);
	ASSERT_TRUE(imf.GetHeaders().empty());
}

TEST(InternetMessageFormat, OnlyNewLineMessage) {
	Imf imf;
	auto[success, offset] = imf.Parse("\r\n");
	ASSERT_EQ(
		true,
		success
	);
	ASSERT_EQ(2, offset);
	ASSERT_TRUE(imf.GetHeaders().empty());
}

TEST(InternetMessageFormat, SingleLineNotTerminated) {
	Imf imf;
	ASSERT_EQ(
		false,
		imf.Parse("From: John Doe").first
	);
}

TEST(InternetMessageFormat, SingleLineTerminated) {
	Imf imf;
	auto[success, offset] = imf.Parse("From: John Doe\r\n\r\n");
	ASSERT_EQ(
		true,
		success
	);
	ASSERT_EQ(18, offset);
}


TEST(InternetMessageFormat, HasHeader) {
	const std::string rawMessage = (
		"From: John Doe <jdoe@machine.example>\r\n"
		"Sender: Michael Jones <mjones@machine.example>\r\n"
		"To: Mary Smith <mary@example.net>\r\n"
		"Subject: Saying Hello\r\n"
		"Date: Fri, 21 Nov 1997 09 : 55 : 06 - 0600\r\n"
		"Message - ID: <1234@local.machine.example>\r\n"
		"\r\n"
		);
	Imf imf;
	auto[success, offset] = imf.Parse(rawMessage);
	ASSERT_EQ(true, success);
	const auto headerCollection = imf.GetHeaders();
	const std::vector< Imf::Header > expectedHeaders{
	{"From", "John Doe <jdoe@machine.example>"},
	{"Sender", "Michael Jones <mjones@machine.example>"},
	{"To", "Mary Smith <mary@example.net>"},
	{"Subject", "Saying Hello"},
	{"Date", "Fri, 21 Nov 1997 09 : 55 : 06 - 0600"},
	{"Message - ID", "<1234@local.machine.example>"},
	};
	ASSERT_EQ(expectedHeaders.size(), headerCollection.size());
	for (size_t i = 0; i < expectedHeaders.size(); ++i) {
		ASSERT_EQ(expectedHeaders[i].m_headerName, headerCollection[i].m_headerName);
		ASSERT_EQ(expectedHeaders[i].m_headerValue, headerCollection[i].m_headerValue);
	}
	ASSERT_TRUE(imf.HasHeader("From"));
	ASSERT_TRUE(imf.HasHeader("Date"));
	ASSERT_TRUE(imf.HasHeader("Message - ID"));
}

TEST(InternetMessageFormat, GetHeaderValue) {
	const std::string rawMessage = (
		"From: John Doe <jdoe@machine.example>\r\n"
		"Sender: Michael Jones <mjones@machine.example>\r\n"
		"To: Mary Smith <mary@example.net>\r\n"
		"Subject: Saying Hello\r\n"
		"Date: Fri, 21 Nov 1997 09 : 55 : 06 - 0600\r\n"
		"Message - ID: <1234@local.machine.example>\r\n"
		"\r\n"
		);
	Imf imf;
	auto[success, offset] = imf.Parse(rawMessage);
	ASSERT_EQ(true, success);
	const auto headerCollection = imf.GetHeaders();
	const std::vector< Imf::Header > expectedHeaders{
	{"From", "John Doe <jdoe@machine.example>"},
	{"Sender", "Michael Jones <mjones@machine.example>"},
	{"To", "Mary Smith <mary@example.net>"},
	{"Subject", "Saying Hello"},
	{"Date", "Fri, 21 Nov 1997 09 : 55 : 06 - 0600"},
	{"Message - ID", "<1234@local.machine.example>"},
	};
	ASSERT_EQ(expectedHeaders.size(), headerCollection.size());
	for (size_t i = 0; i < expectedHeaders.size(); ++i) {
		ASSERT_EQ(expectedHeaders[i].m_headerName, headerCollection[i].m_headerName);
		ASSERT_EQ(expectedHeaders[i].m_headerValue, headerCollection[i].m_headerValue);
	}
	ASSERT_TRUE(imf.HasHeader("Message - ID"));
	ASSERT_EQ("<1234@local.machine.example>", imf.GetHeaderValue("Message - ID"));
}