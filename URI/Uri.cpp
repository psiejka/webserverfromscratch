#include "Uri.h"
#include <iostream>

Uri::Uri()
{
	Clear();
}

Uri::~Uri()
{
}

Uri::Uri(const Uri & other)
{
	//use copy assignment operator
	*this = other;
}

Uri & Uri::operator=(const Uri & other)
{
	m_uri = other.m_uri;
	m_scheme = other.m_scheme;
	m_userName = other.m_userName;
	m_userPass = other.m_userPass;
	m_host = other.m_host;
	m_port = other.m_port;
	m_path = other.m_path;
	m_query = other.m_query;
	m_fragment = other.m_fragment;
	return *this;
}

void Uri::Clear()
{
	m_uri.clear();
	m_scheme.clear();
	m_userName.clear();
	m_userPass.clear();
	m_host.clear();
	m_port.reset();
	m_path.clear();
	m_query.clear();
	m_fragment.clear();
}

bool Uri::Parse(const std::string& uri)
{
	m_uri = uri;
	if (m_uri.length() == 0) {
		return false;
	}

	auto schemeEnd = ParseScheme();
	auto authorityEnd = ParseAuthority(schemeEnd);
	auto pathEnd = ParsePath(authorityEnd);
	auto queryEnd = ParseQuery(pathEnd);
	auto fragmentEnd = ParseFragment(queryEnd);
	return true;
}

std::string::const_iterator Uri::ParseScheme()
{
	//Authority and Path can have ':' character, so it limit search
	auto authorityOrPathDelimiter = std::find(m_uri.begin(), m_uri.end(), '/');

	auto schemeEnd = std::find(m_uri.begin(), authorityOrPathDelimiter, ':');
	if (schemeEnd == m_uri.end()) {
		return schemeEnd;
	}
	else {
		std::string scheme = std::string(m_uri.begin(), schemeEnd);
		//TODO: check if contain any illegal characters
		m_scheme = scheme; // maybe use tolower funtion?
	}
	//Don't want ':' sign
	return schemeEnd + 1;
}

//According to Wikipedia authority contain a userinfo, host and port
std::string::const_iterator Uri::ParseAuthority(
	std::string::const_iterator authorityStart)
{
	std::string::const_iterator authorityEnd = authorityStart;
	auto authorityIt = authorityStart;
	while (authorityEnd != m_uri.end() && *authorityEnd != '?' && *authorityEnd != '#') {
		authorityEnd++;
	}
	if (authorityStart != authorityEnd) {
		if (std::string(authorityStart, authorityStart + 2) == "//") {
			//URI have authority
			authorityIt += 2;
			auto userInfoEnd = std::find(authorityIt, authorityEnd, '@');

			if (userInfoEnd != authorityEnd) {
				authorityIt = ParseUsername(authorityIt, userInfoEnd);
				authorityIt = ParsePassword(authorityIt, userInfoEnd);
			}

			authorityIt = ParseHost(authorityIt, authorityEnd);
			if (*authorityIt == ':')
				authorityIt = ParsePort(authorityIt + 1, authorityEnd);
		}
	}
	return authorityIt;
}

std::string::const_iterator Uri::ParseUsername(
	std::string::const_iterator userInfoStart,
	std::string::const_iterator userInfoEnd)
{
	auto userNameEnd = std::find(userInfoStart, userInfoEnd, ':');
	m_userName = std::string(userInfoStart, userNameEnd);
	return userNameEnd;
}

std::string::const_iterator Uri::ParsePassword(
	std::string::const_iterator userPassStart,
	std::string::const_iterator userInfoEnd)
{
	auto userPasswordEnd = std::find(userPassStart, userInfoEnd, '@');

	if (userPasswordEnd != userPassStart) {
		m_userPass = std::string(userPassStart + 1, userPasswordEnd);
	}
	//without '@' at the end
	return userPasswordEnd + 1;
}

std::string::const_iterator Uri::ParseHost(
	std::string::const_iterator hostStart,
	std::string::const_iterator authorityEnd)
{
	auto hostEnd = hostStart;
	while (hostEnd != authorityEnd) {
		if (*hostEnd == '[') {
			//IPv6
			while (hostEnd != authorityEnd && *hostEnd != ']') {
				hostEnd++;
			}
			if (hostEnd == authorityEnd) {
				//TODO(psiejka): ERROR -> throw?
			}
			//Don't want ']' sign in the next processing
			hostEnd++;
			break;
		}
		else if (*hostEnd == ':' || *hostEnd == '/' || *hostEnd == '?' || *hostEnd == '#') {
			break;
		}
		else {
			hostEnd++;
		}
	}
	m_host = std::string(hostStart, hostEnd);
	return hostEnd;
}

std::string::const_iterator Uri::ParsePort(
	std::string::const_iterator portStart,
	std::string::const_iterator authorityEnd)
{
	auto portEnd = std::find(portStart, authorityEnd, '/');
	m_port = std::stoi(std::string(portStart, portEnd));
	return portEnd;
}

std::string::const_iterator Uri::ParsePath(
	std::string::const_iterator pathStart) {
	auto pathEnd = pathStart;
	while (pathEnd != m_uri.cend() && *pathEnd != '?' && *pathEnd != '#') {
		pathEnd++;
	}
	m_path = std::string(pathStart, pathEnd);
	return pathEnd;
}

std::string::const_iterator Uri::ParseQuery(
	std::string::const_iterator queryStart)
{
	auto queryEnd = queryStart;
	if (queryStart != m_uri.end() && *queryStart == '?') {
		queryStart += 1;
		queryEnd = std::find(queryStart, m_uri.cend(), '#');
	}
	m_query = std::string(queryStart, queryEnd);
	return queryEnd;
}

std::string::const_iterator Uri::ParseFragment(
	std::string::const_iterator fragmentStart)
{
	auto fragmentEnd = m_uri.cend();
	if (fragmentStart != m_uri.end() && *fragmentStart == '#') {
		fragmentStart += 1;
	}
	m_fragment = std::string(fragmentStart, fragmentEnd);
	return m_uri.end();
}

std::string Uri::GetUsername() const
{
	return m_userName;
}

std::string Uri::GetUserPass() const
{
	return m_userPass;
}

std::string Uri::GetScheme() const
{
	return m_scheme;
}

std::string Uri::GetHost() const
{
	return m_host;
}

std::optional<uint16_t> Uri::GetPort() const
{
	return m_port;
}

std::string Uri::GetPath() const
{
	return m_path;
}

std::string Uri::GetQuery() const
{
	return m_query;
}

std::string Uri::GetFragment() const
{
	return m_fragment;
}

bool Uri::operator==(const Uri & uri) const
{
	if (
		this->GetFragment() == uri.GetFragment()
		&& this->GetHost() == uri.GetHost()
		&& this->GetPath() == uri.GetPath()
		&& this->GetQuery() == uri.GetQuery()
		&& this->GetScheme() == uri.GetScheme()
		&& this->GetUsername() == uri.GetUsername()
		&& this->GetUserPass() == uri.GetUserPass()
		&& (
			!this->GetPort().has_value() == !uri.GetPort().has_value())
		||
			this->GetPort().value() == uri.GetPort().value()
		)
	{
		return true;
	}
	return false;
}

