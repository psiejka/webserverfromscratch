#pragma once
#include <string>
#include <optional>
#include <string_view>

/*TODO
- checking for illegal characters
- ParseScheme, ParseAuthority, ParsePath, ParseQuery, ParseFragment should return variant<bool, std::string::const_iterator>.
  False for error, illegal character etc.
*/

//Class that parses the URI as described in https://tools.ietf.org/html/rfc3986
class Uri {
	// scheme:[//[user:password@]domain[:port]][/]path[?query][#fragment]

	public:
		~Uri();
		Uri();
	
		//copy constructor
		Uri(const Uri& other);
		//copy assignment operator
		Uri& operator=(const Uri& other);

		//move constructor
		Uri(Uri&& other) = default;
		//move assignment operator
		Uri& operator=(Uri&& other) = default;

	public:

		//Sets all private members as if they were never used.
		void Clear();

		//Builds the URI from string passed to the method
		bool Parse(const std::string& uri);

		//Returns the username of the URI
		std::string GetUsername() const;
		
		//Returns the password of user
		std::string GetUserPass() const;

		//Returns the scheme of the URI
		std::string GetScheme() const;

		//Returns the host of the URI
		std::string GetHost() const;

		//Returns the port of the URI.
		std::optional<uint16_t> GetPort() const;

		//Returns the path of the URI
		std::string GetPath() const;

		//Returns the query of the URI
		std::string GetQuery() const;

		//Returns the fragment of the URI
		std::string GetFragment() const;

		//Equality comparison operator for this class.
		bool operator==(const Uri& uri) const;

	private:
		//Parses the scheme of the URI and returns pointer to the end of this element
		std::string::const_iterator ParseScheme();
		//Parses the authority of the URI and returns pointer to the end of this element
		std::string::const_iterator ParseAuthority(
			std::string::const_iterator schemeEnd);
		//Parses the username of the URI and returns pointer to the end of this element
		std::string::const_iterator ParseUsername(
			std::string::const_iterator userInfoStart,
			std::string::const_iterator userInfoEnd);
		//Parses the password of the URI and returns pointer to the end of this element
		std::string::const_iterator ParsePassword(
			std::string::const_iterator userInfoStart,
			std::string::const_iterator userInfoEnd);
		//Parses the host of the URI and returns pointer to the end of this element
		std::string::const_iterator ParseHost(
			std::string::const_iterator hostStart,
			std::string::const_iterator authorityEnd);
		//Parses the port of the URI and returns pointer to the end of this element
		std::string::const_iterator ParsePort(
			std::string::const_iterator portStart,
			std::string::const_iterator authorityEnd);
		//Parses the path of the URI and returns pointer to the end of this element
		std::string::const_iterator ParsePath(
			std::string::const_iterator pathStart);
		//Parses the query of the URI and returns pointer to the end of this element
		std::string::const_iterator ParseQuery(
			std::string::const_iterator queryStart);
		//Parses the fragment of the URI and returns pointer to the end of this element
		std::string::const_iterator ParseFragment(
			std::string::const_iterator fragmentStart);

	private:
		std::string m_uri;
		std::string m_scheme;
		std::string m_userName;
		std::string m_userPass;
		std::string m_host;
		std::optional<uint16_t> m_port;
		std::string m_path;
		std::string m_query;
		std::string m_fragment;
};
