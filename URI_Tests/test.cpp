#include "pch.h"
#include "../Uri/Uri.h"

/*TEST(TestCaseName, TestName) {
	EXPECT_EQ(1, 1);
	EXPECT_TRUE(true);
}*/

TEST(UriTests, httpsUsernameHostPort) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("https://john.doe@www.example.com:123/"));
	ASSERT_EQ("https", uri.GetScheme());
	ASSERT_EQ("john.doe", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("www.example.com", uri.GetHost());
	ASSERT_EQ(123, uri.GetPort().value());
}

TEST(UriTests, httpsUsernameHostTrickyPort1) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("https://john.doe@www.example.com:123"));
	ASSERT_EQ("https", uri.GetScheme());
	ASSERT_EQ("john.doe", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("www.example.com", uri.GetHost());
	ASSERT_EQ(123, uri.GetPort().value());
}

TEST(UriTests, httpsUsernameHostTrickyPortQuery) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("https://john.doe@www.example.com:123?q=2"));
	ASSERT_EQ("https", uri.GetScheme());
	ASSERT_EQ("john.doe", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("www.example.com", uri.GetHost());
	ASSERT_EQ(123, uri.GetPort().value());
	ASSERT_EQ("q=2", uri.GetQuery());
}

TEST(UriTests, httpsUsernameHostTrickyPortQueryFragment) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("https://john.doe@www.example.com:123?q=2#fragment"));
	ASSERT_EQ("https", uri.GetScheme());
	ASSERT_EQ("john.doe", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("www.example.com", uri.GetHost());
	ASSERT_EQ(123, uri.GetPort().value());
	ASSERT_EQ("q=2", uri.GetQuery());
	ASSERT_EQ("fragment", uri.GetFragment());
}

TEST(UriTests, httpsUsernameHostPortPath) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("https://john.doe@www.example.com:123/a/b/c"));
	ASSERT_EQ("https", uri.GetScheme());
	ASSERT_EQ("john.doe", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("www.example.com", uri.GetHost());
	ASSERT_EQ(123, uri.GetPort().value());
	ASSERT_EQ("/a/b/c", uri.GetPath());
}

TEST(UriTests, httpsUsernameHostPortPathQuery) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("https://john.doe@www.example.com:123/a/b/c?q=2"));
	ASSERT_EQ("https", uri.GetScheme());
	ASSERT_EQ("john.doe", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("www.example.com", uri.GetHost());
	ASSERT_EQ(123, uri.GetPort().value());
	ASSERT_EQ("/a/b/c", uri.GetPath());
	ASSERT_EQ("q=2", uri.GetQuery());
}

TEST(UriTests, httpsUsernamePasswordHostPortPathQueryFragment) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("https://john.doe:s3cr3tP4ss@www.example.com:123/a/b/c?q=2#frag"));
	ASSERT_EQ("https", uri.GetScheme());
	ASSERT_EQ("john.doe", uri.GetUsername());
	ASSERT_EQ("s3cr3tP4ss", uri.GetUserPass());
	ASSERT_EQ("www.example.com", uri.GetHost());
	ASSERT_EQ(123, uri.GetPort().value());
	ASSERT_EQ("/a/b/c", uri.GetPath());
	ASSERT_EQ("q=2", uri.GetQuery());
	ASSERT_EQ("frag", uri.GetFragment());
}

TEST(UriTests, SchemeUsernamePasswordHostPortPathQueryFragment) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("https://john.doe:abc@www.example.com:123/a/b/c?q=2#frag"));
	ASSERT_EQ("https", uri.GetScheme());
	ASSERT_EQ("john.doe", uri.GetUsername());
	ASSERT_EQ("abc", uri.GetUserPass());
	ASSERT_EQ("www.example.com", uri.GetHost());
	ASSERT_EQ(123, uri.GetPort().value());
	ASSERT_EQ("/a/b/c", uri.GetPath());
	ASSERT_EQ("q=2", uri.GetQuery());
	ASSERT_EQ("frag", uri.GetFragment());
}

TEST(UriTests, httpsHostPortPathQueryFragment) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("https://www.example.com:123/a/b/c?q=2#frag"));
	ASSERT_EQ("https", uri.GetScheme());
	ASSERT_EQ("", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("www.example.com", uri.GetHost());
	ASSERT_EQ(123, uri.GetPort().value());
	ASSERT_EQ("/a/b/c", uri.GetPath());
	ASSERT_EQ("q=2", uri.GetQuery());
	ASSERT_EQ("frag", uri.GetFragment());
}

TEST(UriTests, mailtoPath) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("mailto:John.Doe@example.com"));
	ASSERT_EQ("mailto", uri.GetScheme());
	ASSERT_EQ("", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("", uri.GetHost());
	ASSERT_FALSE(uri.GetPort().has_value());
	ASSERT_EQ("John.Doe@example.com", uri.GetPath());
	ASSERT_EQ("", uri.GetQuery());
	ASSERT_EQ("", uri.GetFragment());
}

TEST(UriTests, ldapIPv6PathQuery) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("ldap://[2001:db8::7]/c=GB?objectClass?one"));
	ASSERT_EQ("ldap", uri.GetScheme());
	ASSERT_EQ("", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("[2001:db8::7]", uri.GetHost());
	ASSERT_FALSE(uri.GetPort().has_value());
	ASSERT_EQ("/c=GB", uri.GetPath());
	ASSERT_EQ("objectClass?one", uri.GetQuery());
	ASSERT_EQ("", uri.GetFragment());
}

TEST(UriTests, urnPath) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("urn:oasis:names:specification:docbook:dtd:xml:4.1.2"));
	ASSERT_EQ("urn", uri.GetScheme());
	ASSERT_EQ("", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("", uri.GetHost());
	ASSERT_FALSE(uri.GetPort().has_value());
	ASSERT_EQ("oasis:names:specification:docbook:dtd:xml:4.1.2", uri.GetPath());
	ASSERT_EQ("", uri.GetQuery());
	ASSERT_EQ("", uri.GetFragment());
}

TEST(UriTests, TelnetIPv4Port) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("telnet://192.0.2.16:80/"));
	ASSERT_EQ("telnet", uri.GetScheme());
	ASSERT_EQ("", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("192.0.2.16", uri.GetHost());
	ASSERT_EQ(80, uri.GetPort().value());
	ASSERT_EQ("/", uri.GetPath());
	ASSERT_EQ("", uri.GetQuery());
	ASSERT_EQ("", uri.GetFragment());
}

TEST(UriTests, telPath) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("tel:+1-816-555-1212"));
	ASSERT_EQ("tel", uri.GetScheme());
	ASSERT_EQ("", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("", uri.GetHost());
	ASSERT_FALSE(uri.GetPort().has_value());
	ASSERT_EQ("+1-816-555-1212", uri.GetPath());
	ASSERT_EQ("", uri.GetQuery());
	ASSERT_EQ("", uri.GetFragment());
}

TEST(UriTests, httpsUsernameHostPortPathQueryFragment) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("https://john.doe@www.example.com:123/forum/questions/?tag=networking&order=newest#top"));
	ASSERT_EQ("https", uri.GetScheme());
	ASSERT_EQ("john.doe", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("www.example.com", uri.GetHost());
	ASSERT_EQ(123, uri.GetPort().value());
	ASSERT_EQ("/forum/questions/", uri.GetPath());
	ASSERT_EQ("tag=networking&order=newest", uri.GetQuery());
	ASSERT_EQ("top", uri.GetFragment());
}

TEST(UriTests, EmptyUri) {
	Uri uri;
	ASSERT_FALSE(uri.Parse(""));
	ASSERT_EQ("", uri.GetScheme());
	ASSERT_EQ("", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("", uri.GetHost());
	ASSERT_FALSE(uri.GetPort().has_value());
	ASSERT_EQ("", uri.GetPath());
	ASSERT_EQ("", uri.GetQuery());
	ASSERT_EQ("", uri.GetFragment());
}

TEST(UriTests, FTPwithPath) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("ftp://ftp.is.co.za/rfc/rfc1808.txt"));
	ASSERT_EQ("ftp", uri.GetScheme());
	ASSERT_EQ("", uri.GetUsername());
	ASSERT_EQ("", uri.GetUserPass());
	ASSERT_EQ("ftp.is.co.za", uri.GetHost());
	ASSERT_FALSE(uri.GetPort().has_value());
	ASSERT_EQ("/rfc/rfc1808.txt", uri.GetPath());
	ASSERT_EQ("", uri.GetQuery());
	ASSERT_EQ("", uri.GetFragment());
}

TEST(UriTests, LocalhostPortQuery) {
	Uri uri;
	ASSERT_TRUE(uri.Parse("http://localhost:8080?&foo=1"));
	ASSERT_EQ("http", uri.GetScheme());
	ASSERT_EQ("localhost", uri.GetHost());
	ASSERT_EQ(8080, uri.GetPort().value());
	ASSERT_EQ("", uri.GetPath());
	ASSERT_EQ("&foo=1", uri.GetQuery());
}
