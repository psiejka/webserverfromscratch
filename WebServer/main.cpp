#include "../HTTP/Server.h"
#include <iostream>

//TODO:

class MyServer : public Server {
	std::string ProcessReceivedData(std::string_view request) {
		const std::string body = "<center>It's my own 404 error.<br><p><h1>404</h1></p></center>";
		const std::string response =
			"HTTP/1.1 404 Not Found\r\n"
			"Content-Length: 64\r\n"
			"Content-Type: text/html\r\n"
			"\r\n"
			+ body + "\r\n";

		return response;
	}
};

int main(int argc, char* argv[]) {
	bool shutDown = false;
	std::string input;

	std::cout << "SERVER: START\n";

	Server server;
	server.SetContentPath("www/");
	server.Run(8080);

	while (!shutDown) {
		std::getline(std::cin, input);
		if (input == "exit") {
			server.Stop();
			shutDown = true;
		}
		else {
			std::cout << "I don't know that command.\n";
		}
		input.clear();
	}
	std::cout << "Closing every thread... Just wait a moment.\n";

	return 0;
}